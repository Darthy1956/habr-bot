import asyncio
import os

from aiogram import Bot, Dispatcher, types
from routers import start, top_habr


TOKEN_BOT =API_TOKEN = '5525116066:AAFomkv1V5ewMxKxM81FYAj-aYkReF18gZg'

    #os.environ.get('TOKEN_BOT') if os.environ.get('TOKEN_BOT') else '5507404109:AAGQ9s5EuyQgIqTsqb4tT4aL4KoQk3Q7Rk8'


async def launch_bot(token: str):
    # Объект бота
    bot = Bot(token=token,parse_mode="HTML")
    # Диспетчер
    dp = Dispatcher(bot=bot)

    dp.include_router(start.router)
    dp.include_router(top_habr.router)

    # Запускаем бота и пропускаем все накопленные входящие
    # Да, этот метод можно вызвать даже если у вас поллинг
    await bot.delete_webhook(drop_pending_updates=True)
    await dp.start_polling(bot)


if __name__ == "__main__":
    bot: Bot = asyncio.run(launch_bot(TOKEN_BOT))
