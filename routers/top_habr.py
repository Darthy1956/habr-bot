import requests
from aiogram.utils.keyboard import InlineKeyboardBuilder
from aiogram.filters.text import Text
from aiogram import Router, Dispatcher, types
from aiogram.filters import Command
from aiogram.types import Message, ReplyKeyboardRemove
from bs4 import BeautifulSoup
import urllib.request
import re
import aiogram.utils.markdown as md

router = Router()


def get_top_habr(depth: int = 5) -> str:
    url = 'https://habr.com/ru/top/monthly/'
    response = requests.get(url)
    if response.ok:
        code = urllib.request.urlopen(url)
        soup = BeautifulSoup(code, 'lxml')
        body = soup.find('body')
        links = []
        names = []
        top_articles = body.find_all('h2', {"class": 'tm-article-snippet__title tm-article-snippet__title_h2'})
        for article in top_articles:
            article_block = article.find('a')
            links.append(article_block.get('href'))
            name = article_block.find('span')
            str_name = str(name)
            str_name = re.sub(r'<.*?>', "", str_name)
            names.append(str_name)
        html_builder = "Выбери статью, которую хочешь открыть:"
        for idx,(l, n) in enumerate(zip(links[:depth], names[:depth])):
            html_builder = html_builder + f"\n🟢 {idx+1}. {n}\n<a href=\"https://habr.com{l}\">открыть</a>"
        return html_builder
    return ""


@router.message(Command(commands=['show_top_habr']))
async def how_many(message: Message):
    kb = InlineKeyboardBuilder()
    kb = [
         [types.KeyboardButton(text="топ 5")],
         [types.KeyboardButton(text="топ 7")],
         [types.KeyboardButton(text="топ 10")]
    ]
    keyboard = types.ReplyKeyboardMarkup(keyboard=kb)
    await message.answer("вывести:", reply_markup=keyboard)

@router.message(Text(text = 'топ 5'))
async def show_top_5(message: Message):
    await message.answer(get_top_habr(5), disable_web_page_preview=True, reply_markup=ReplyKeyboardRemove())

@router.message(Text(text = 'топ 7'))
async def show_top_5(message: Message):
    await message.answer(get_top_habr(7), disable_web_page_preview=True, reply_markup=ReplyKeyboardRemove())

@router.message(Text(text = 'топ 10'))
async def show_top_5(message: Message):
    await message.answer(get_top_habr(10), disable_web_page_preview=True, reply_markup=ReplyKeyboardRemove())
