from aiogram import Router
from aiogram.filters import Command
from aiogram.types import Message
from aiogram import Bot, Dispatcher

router = Router()

@router.message(Command(commands=["start"]))
async def command_start(message: Message) -> None:
    await message.answer(
        "Привет!\nЯ могу показать, топ популярных статей на HABRe за последний месяц, "
        "просто введи команду /show_top_habr"
    )